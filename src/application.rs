use gtk::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gio, glib};

use crate::config::VERSION;
use crate::QuillWindow;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct QuillApplication {}

    #[glib::object_subclass]
    impl ObjectSubclass for QuillApplication {
        const NAME: &'static str = "QuillApplication";
        type Type = super::QuillApplication;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for QuillApplication {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();
            obj.setup_gactions();
            obj.set_accels_for_action("app.quit", &["<primary>q"]);
        }
    }

    impl ApplicationImpl for QuillApplication {
        fn activate(&self) {
            let application = self.obj();
            let window = if let Some(window) = application.active_window() {
                window
            } else {
                let window = QuillWindow::new(&*application);
                window.upcast()
            };

            window.present();
        }
    }

    impl GtkApplicationImpl for QuillApplication {}
    impl AdwApplicationImpl for QuillApplication {}
}

glib::wrapper! {
    pub struct QuillApplication(ObjectSubclass<imp::QuillApplication>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl QuillApplication {
    pub fn new(application_id: &str, flags: &gio::ApplicationFlags) -> Self {
        glib::Object::builder()
            .property("application-id", application_id)
            .property("flags", flags)
            .build()
    }

    fn setup_gactions(&self) {
        let quit_action = gio::ActionEntry::builder("quit")
            .activate(move |app: &Self, _, _| app.quit())
            .build();
        let about_action = gio::ActionEntry::builder("about")
            .activate(move |app: &Self, _, _| app.show_about())
            .build();
        self.add_action_entries([quit_action, about_action]);
    }

    fn show_about(&self) {
        let window = self.active_window().unwrap();
        let about = adw::AboutWindow::builder()
            .transient_for(&window)
            .application_name("Quill")
            .application_icon("io.gitlab.snailonacliff.Quill")
            .developer_name("Kira K")
            .version(VERSION)
            .developers(vec!["Kira K"])
            .copyright("© 2024 Kira K")
            .build();

        about.present();
    }
}
