mod application;
mod config;
mod window;

use self::application::QuillApplication;
use self::window::QuillWindow;

use config::{GETTEXT_PACKAGE, LOCALEDIR, PKGDATADIR};
use gettextrs::{bind_textdomain_codeset, bindtextdomain, textdomain};
use gtk::{gio, glib};
use gtk::prelude::*;

fn main() -> glib::ExitCode {
    // gettext
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR).expect("Unable to bind the text domain");
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8")
        .expect("Unable to set the text domain encoding");
    textdomain(GETTEXT_PACKAGE).expect("Unable to switch to the text domain");

    // resources
    let resources = gio::Resource::load(PKGDATADIR.to_owned() + "/quill.gresource")
        .expect("Could not load resources");
    gio::resources_register(&resources);

    let app = QuillApplication::new("io.gitlab.snailonacliff.Quill", &gio::ApplicationFlags::empty());
    app.run()
}
